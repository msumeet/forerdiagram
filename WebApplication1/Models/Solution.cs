﻿namespace WebApplication1.Models
{
    public class Solution : BaseModel
    {
        public string Discription { get; set; }
        public string Detail { get; set; }
        public int Code { get; set; }
        public string Image { get; set; }
    }
}
