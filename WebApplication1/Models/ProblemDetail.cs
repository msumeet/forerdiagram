﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class ProblemDetail : BaseModel
    {
        public string Description { get; set; }
        public string ErrorCode { get; set; }
        public string Technology { get; set; }
        public string Image { get; set; }
        [ForeignKey("Solution")]
        public Guid SolutionId { get; set; }
        public virtual Solution Solution { get; set; }
    }
}
