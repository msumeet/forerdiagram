﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class Problem : BaseModel
    {
        public string Title { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("ProblemDetail")]
        public Guid ProblemDetailId { get; set; }
        public virtual ProblemDetail ProblemDetail { get; set; }
        [ForeignKey("Tag")]
        public Guid TagId { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
